### 局域网Server和Client

#### 1.如何运行

运行Server，你需要Python2.7的环境。
切换到server目录下，执行命令，启动server：

```shell
python server.py
```

以client-python为例，运行Client-python，你需要Python3.5以上版本的环境。
打开两个窗口，切换到client目录下，执行命令，启动client-A和client-B：

```shell
python main.py
```

#### 2.修改配置

server：修改server/server.py中HOST和PORT即可。

client：以python的client为例，修改client-python/config.py中Config类即可。