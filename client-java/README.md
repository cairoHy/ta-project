### Client-JAVA

#### 1.关于Client-JAVA

模拟棋类游戏向Server的基本请求和处理流程，JAVA语言DEMO。

环境：JDK1.8-64位

#### 2.如何运行

根据Server修改src/main/Config.java文件中Config类的hostIP和PORT信息。

编译client-java工程，并两次执行src/main/Main.java中的主函数（模拟两个玩家客户端）即可，推荐使用Eclipse或者Intellij这种集成环境，当然，也可以用下面的命令：

```shell
java main.Main
```

