package main;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.net.Socket;
import java.nio.charset.StandardCharsets;

class Agent {

    private AgentStatus agentStatus;
    private Config nz;
    private GameInfo gameInfo;
    int curStep;
    private Socket socket = null;
    private String curMsg;
    private DataOutputStream dOut;

    Agent() throws IOException {
        this.agentStatus = new AgentStatus();  // 当前Client的状态;
        this.nz = new Config();

        // 建立socket连接;
        this.socket = new Socket(this.nz.hostIP, this.nz.port);
        this.dOut = new DataOutputStream(this.socket.getOutputStream());
        this.curMsg = null;
        // 存储对局信息;
        this.gameInfo = new GameInfo();
        this.curStep = 0;
    }

    status curStatus() {
        return this.agentStatus.curStatus();
    }

    /* 关闭socket连接 */
    void closeSocket() throws IOException {
        System.out.println("关闭socket.");
        this.dOut.close();
        this.socket.close();
    }

    /* 发送消息到远程socket（服务器），发送的是通过UTF-8编码的二进制byte流 */
    private void send(String msg) throws IOException {
        this.dOut.write(msg.getBytes(StandardCharsets.UTF_8));
        this.dOut.flush();
    }

    /* 从远程socket（服务器）接收信息，接收到byte格式的二进制信息，而后通过UTF-8编码转化为String */
    void receive() throws IOException {
        DataInputStream oin = new DataInputStream(this.socket.getInputStream());
        byte[] msg = new byte[this.nz.bufSize];
        //noinspection ResultOfMethodCallIgnored
        oin.read(msg);
        this.curMsg = new String(msg, StandardCharsets.UTF_8);
    }

    /* 向服务器表明自己出于空闲等待状态，同时声明游戏类型 */
    void ask_for_game() throws IOException {
        String msg = Json.createObjectBuilder()
                .add("type", 0)
                .add("msg", Json.createObjectBuilder()
                        .add("id", this.nz.gameTypeId)
                        .add("name", this.nz.nickName))
                .build()
                .toString();
        this.send(msg);
        System.out.println("请求加入对战.");
        System.out.println(msg);
        this.agentStatus.changeStatus(status.WAIT_FOR_GAME);
    }

    /* 分析返回的游戏配对信息 */
    private void analyse_match_msg(String msg) {
        System.out.println(msg);
        JsonReader jsonReader = Json.createReader(new StringReader(msg));
        JsonObject parsed_json = jsonReader.readObject();
        jsonReader.close();
        if (parsed_json.getInt("status") == 1) {
            System.out.println("配对成功");
            this.gameInfo = new GameInfo(parsed_json.getString("counterpart_name"), parsed_json.getString("game_id"), parsed_json.getInt("side"));
            this.curStep = 0;
            System.out.println(this.gameInfo.toString());
            status new_status = this.gameInfo.side == 1 ? status.MY_TURN : status.WAIT_FOR_ACTION;
            this.agentStatus.changeStatus(new_status);
        } else {
            System.out.println("配对失败，原因：配对超时.退回初始状态并退出.");
            System.exit(-1);
        }
    }

    /* 分析返回的socket信息，根据所处的status做出不同动作 */
    void analyse() throws IOException {
        switch (this.curStatus()) {
            case WAIT_FOR_GAME:
                this.analyse_match_msg(this.curMsg);
                break;
            case WAIT_FOR_ACTION:
                this.analyse_action_msg(this.curMsg);
                break;
            case START:
            case MY_TURN:
            case GAME_END:
                this.just_print_msg(this.curMsg);
                break;
        }
    }

    /* 对手的回合信息接收到了，分析一下 */
    private void analyse_action_msg(String msg) throws IOException {
        this.curStep += 1;
        System.out.println("接收Action：");
        System.out.println(msg);
        JsonReader jsonReader = Json.createReader(new StringReader(msg));
        JsonObject parsed_json = jsonReader.readObject();
        jsonReader.close();
        /*
        # -----------------------------------
        # 继续解析json信息，进行坐标变换，刷新GUI
        # some code here...
        # -----------------------------------
        */
        if (parsed_json.containsKey("game_status") && parsed_json.getString("game_status").equals("exit")) {
            this.quit_verify();
            this.agentStatus.changeStatus(status.GAME_END);
        } else {
            this.agentStatus.changeStatus(status.MY_TURN);
        }
    }

    /* 接收到游戏结束消息时，发送到Server的确认信息 */
    private void quit_verify() throws IOException {
        String msg = Json.createObjectBuilder()
                .add("type", 4)
                .build()
                .toString();
        this.send(msg);
    }

    /* 在某些状态下接受到信息后，只是print信息 */
    private void just_print_msg(String msg) {
        System.out.println("处在某些不需要接受信息的状态" + this.curStatus() + "，接受到 " + msg);
    }

    /* 发送请求，退出游戏，当客户端判定游戏已经结束时，也要向服务端发送该请求 */
    void quit_game() throws IOException {
        String msg = Json.createObjectBuilder()
                .add("type", 3)
                .add("msg", Json.createObjectBuilder()
                        .add("request", "exit")
                        .add("game_id", this.gameInfo.gameId)
                        .add("side", this.gameInfo.side))
                .build()
                .toString();
        this.send(msg);
        this.agentStatus.changeStatus(status.GAME_END);
    }

    /* 终于到了我的回合了，下子或者移动，然后把行动信息发送到服务器 */
    void do_and_send_action() throws IOException {
        this.curStep += 1;
        String action = Utils.gen_action_msg(this.gameInfo.gameId, this.gameInfo.side);
        this.send(action);
        System.out.println("发送action：\n" + action);
        this.agentStatus.changeStatus(status.WAIT_FOR_ACTION);
    }
}
