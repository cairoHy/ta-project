package main;

enum status {
    None(0), START(1), WAIT_FOR_GAME(2), WAIT_FOR_ACTION(3), MY_TURN(4), GAME_END(5);
    private final int value;
    private final String[] statusArray = {"无状态", "开始状态", "等待游戏", "等待行动（对手回合）", "己方回合", "游戏结束"};

    status(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return statusArray[value];
    }
}

public class AgentStatus {
    private status curStatus;
    private status prevStatus;

    AgentStatus() {
        prevStatus = status.None;
        curStatus = status.START;
    }

    status curStatus(){
        return this.curStatus;
    }

    void changeStatus(status new_status) {
        this.prevStatus = this.curStatus;
        this.curStatus = new_status;
        System.out.println(this.toString());
    }

    @Override
    public String toString() {
        return "Client 状态：" + prevStatus.toString() + "->" + curStatus.toString();
    }
}
