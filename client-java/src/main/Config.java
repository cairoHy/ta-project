package main;

class Config {
    // --------------socket相关配置-----------------;
    int bufSize;
    String hostIP;
    int port;
    // --------------client相关配置-----------------;
    String nickName;
    int gameTypeId;

    Config() {
        hostIP = "127.0.0.1";
        bufSize = 1024;
        port = 50005;
        nickName = "小明";
        gameTypeId = 1;
    }
}
