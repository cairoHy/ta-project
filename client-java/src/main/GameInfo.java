package main;

/* 游戏局信息存储类 */
public class GameInfo {
    private String counterpartName = null;

    int side;
    String gameId = null;

    GameInfo() {
    }

    GameInfo(String name, String gameId, int side) {
        this.counterpartName = name;
        this.gameId = gameId;
        this.side = side;
    }

    @Override
    public String toString() {
        return this.gameId == null ? "当前游戏局尚未开始" : "当前游戏局信息：\n\t游戏局ID：" + this.gameId + "\n\t对手昵称：" + this.counterpartName + "\n\t是否先手（红方黑方）：" + this.side;
    }

}
