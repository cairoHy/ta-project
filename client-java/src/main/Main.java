package main;

import java.io.IOException;
import java.util.concurrent.ThreadLocalRandom;

public class Main {
    /* 客户端主程序，调用各种logic，修改当前的状态 */
    public static void main(String[] args) {
        try {
            Agent agent = new Agent();
            agent.ask_for_game();
            agent.receive();
            agent.analyse();  // 这里确保能够进入游戏
            while (true) {
                System.out.println("-----------------------" + "第" + agent.curStep + "步" + "-----------------------");
                switch (agent.curStatus()) {
                    case MY_TURN:
                        if (agent.curStep > 10 && ThreadLocalRandom.current().nextInt(1, 100) > 30) {// 一定步数后请求退出游戏
                            agent.quit_game();
                        } else {
                            agent.do_and_send_action();
                        }
                        break;
                    case WAIT_FOR_ACTION:
                        agent.receive();
                        agent.analyse();
                        break;
                    case GAME_END:
                        System.out.println("游戏结束");
                        agent.closeSocket();
                        System.exit(0);
                        break;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
