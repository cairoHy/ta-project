package main;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.IntStream;

public class Utils {
    private static int min = 0;
    private static int max = 20;

    /* 落子，数据产生 */
    private static JsonObject gen_internal1(String game_id, int side) {
        JsonObject res;
        JsonObjectBuilder jo = Json.createObjectBuilder()
                .add("game_id", game_id)
                .add("side", side);
        if (ThreadLocalRandom.current().nextInt(1, 2 + 1) > 1) {// 落1子
            res = jo.add("x", ThreadLocalRandom.current().nextInt(min, max + 1))
                    .add("y", ThreadLocalRandom.current().nextInt(min, max + 1))
                    .build();
        } else {// 用lambda表达式，落2-7子
            IntStream.range(2, 7).forEach(
                    n -> jo.add(String.valueOf(n), Json.createObjectBuilder()
                            .add("x", ThreadLocalRandom.current().nextInt(min, max + 1))
                            .add("y", ThreadLocalRandom.current().nextInt(min, max + 1))
                    ));
            res = jo.build();
        }

        return res;
    }

    /* 动子，数据产生 */
    private static JsonObject gen_internal2(String game_id, int side) {
        JsonObject res;
        JsonObjectBuilder jo = Json.createObjectBuilder()
                .add("game_id", game_id)
                .add("side", side);
        if (ThreadLocalRandom.current().nextInt(1, 2 + 1) > 1) {// 动1子
            res = jo.add("src", Json.createObjectBuilder()
                    .add("x", ThreadLocalRandom.current().nextInt(min, max + 1))
                    .add("y", ThreadLocalRandom.current().nextInt(min, max + 1)))
                    .add("dest", Json.createObjectBuilder()
                            .add("x", ThreadLocalRandom.current().nextInt(min, max + 1))
                            .add("y", ThreadLocalRandom.current().nextInt(min, max + 1)))
                    .build();
        } else {// 用lambda表达式，动2-7子
            IntStream.range(2, 7).forEach(
                    n -> jo.add(String.valueOf(n), Json.createObjectBuilder()
                            .add("src", Json.createObjectBuilder()
                                    .add("x", ThreadLocalRandom.current().nextInt(min, max + 1))
                                    .add("y", ThreadLocalRandom.current().nextInt(min, max + 1)))
                            .add("dest", Json.createObjectBuilder()
                                    .add("x", ThreadLocalRandom.current().nextInt(min, max + 1))
                                    .add("y", ThreadLocalRandom.current().nextInt(min, max + 1)))
                    ));
            res = jo.build();
        }

        return res;
    }

    /* 这里产生当前回合的动作(随机产生的)，实际客户端需要根据点击事件确定，记得做坐标转换 */
    static String gen_action_msg(String game_id, int side) {
        // 模拟落1子或者落2-7子
        JsonObject drop = Json.createObjectBuilder()
                .add("msg", gen_internal1(game_id, side))
                .add("type", 1)
                .build();
        // 模拟动1子或者动2-7子
        JsonObject move = Json.createObjectBuilder()
                .add("msg", gen_internal2(game_id, side))
                .add("type", 2)
                .build();
        return ThreadLocalRandom.current().nextInt(1, 2 + 1) > 1 ? drop.toString() : move.toString();
    }

    public static void main(String[] args) {
        IntStream.range(2, 7).forEach(
                i -> System.out.println(Utils.gen_action_msg("1234", 1))
        );
    }
}


