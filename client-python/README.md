### Client-python

#### 0.关于Client-python

模拟棋类游戏向Server的基本请求和处理流程，Python语言DEMO。

#### 1.如何运行

根据Server修改Config.py中Config类的IP和PORT信息。

在client目录下执行如下命令即可：
```python
python main.py
```
