# coding:utf-8
import json
import socket
from pprint import pprint

from config import *
from utils import enc, dec, GameInfo, gen_action_msg


class ClientAgent:
    """Client的Agent"""

    def __init__(self):
        self.status = Status()  # 当前Client的状态
        self.nz = Config
        self.server = (self.nz.host, self.nz.port)
        # 建立socket连接
        self._socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._socket.connect(self.server)
        self.cur_msg = None
        # 存储对局信息
        self.game_info = GameInfo()
        self.cur_step = 0

    def close_socket(self):
        """结束时，关闭socket连接"""
        print('关闭socket.')
        self._socket.close()

    def send(self, msg):
        """向Server发送socket信息，发送前encode"""
        json_msg = json.dumps(msg)
        self._socket.send(enc(json_msg))

    def receive(self):
        """从Server接收socket信息，并decode为string"""
        self.cur_msg = dec(self._socket.recvfrom(self.nz.buf_size)[0])

    def ask_for_game(self):
        """向服务器表明自己出于空闲等待状态，同时声明游戏类型"""
        msg = {
            'type': 0,
            'msg': {
                'id': self.nz.game_id,
                'name': self.nz.nick_name
            }
        }
        self.send(msg)
        print('请求加入对战.')
        pprint(msg)
        self.status.change_status(WAIT_FOR_GAME)

    def analyse_match_msg(self, msg):
        """分析返回的游戏配对信息"""
        parse_json = json.loads(msg, encoding='utf-8')
        print('分析配对信息：')
        pprint(parse_json)
        try:
            if parse_json['status'] == 1:
                print('配对成功.')
                self.game_info = GameInfo(parse_json['counterpart_name'],
                                          parse_json['game_id'],
                                          parse_json['side'])
                self.cur_step = 0
                print(self.game_info)
                new_status = MY_TURN if self.game_info.side else WAIT_FOR_ACTION
                self.status.change_status(new_status)
            else:
                print('配对失败，原因：配对超时.退回初始状态并退出.')
                self.status.change_status(START)
                exit(-1)
        except Exception as why:
            print('解析json失败，原因：{}\n退回初始状态并退出.'.format(why))
            self.status.change_status(START)
            exit(-1)

    def just_print_msg(self, msg):
        """在某些状态下接受到信息后，只是print信息"""
        pprint('处在某些不需要接受信息的状态{}，接受到 {}'.format(self.status.cur_status, json.dumps(msg)))

    def quit_game(self):
        """发送请求，退出游戏，当客户端判定游戏已经结束时，也要向服务端发送该请求"""
        msg = {
            'type': 3,
            'msg': {
                'request': 'exit',
                'game_id': self.game_info.game_id,
                'side': self.game_info.side
            }
        }
        self.send(msg)
        self.status.change_status(GAME_END)

    def quit_verify(self):
        """接收到游戏结束消息时，发送到Server的确认信息"""
        msg = {
            'type': 4
        }
        self.send(msg)

    def do_and_send_action(self):
        """终于到了我的回合了，下子或者移动"""
        self.cur_step += 1
        msg = gen_action_msg(self.game_info.game_id, self.game_info.side)
        self.send(msg)
        print('发送Action：')
        pprint(msg)
        self.status.change_status(WAIT_FOR_ACTION)

    def analyse_action_msg(self, msg):
        """对手的回合信息接收到了，分析一下"""
        self.cur_step += 1
        parsed_json = json.loads(msg, encoding='utf-8')
        print('接收Action：')
        pprint(parsed_json)
        # -----------------------------------
        # 继续解析json信息，进行坐标变换，刷新GUI
        # some code here...
        # -----------------------------------
        if 'game_status' in parsed_json and parsed_json['game_status'] == 'exit':
            self.quit_verify()
            self.status.change_status(GAME_END)
        else:
            self.status.change_status(MY_TURN)

    def analyse(self):
        """分析返回的socket信息，根据所处的status做出不同动作"""
        dispense = {
            START: self.just_print_msg,
            WAIT_FOR_GAME: self.analyse_match_msg,
            WAIT_FOR_ACTION: self.analyse_action_msg,
            MY_TURN: self.just_print_msg,
            GAME_END: self.just_print_msg
        }
        dispense.get(self.status.cur_status)(self.cur_msg)
