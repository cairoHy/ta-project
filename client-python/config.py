# coding:utf-8

START = 0
WAIT_FOR_GAME = 1
WAIT_FOR_ACTION = 2
MY_TURN = 3
GAME_END = 4
STATUS_REPR = ['开始状态', '等待游戏', '等待行动（对手回合）', '己方回合', '游戏结束']


class Status:
    """表明client当前所处状态的常量"""

    def __init__(self):
        self.cur_status = START
        self.prev_status = None

    def change_status(self, new_status):
        self.prev_status = self.cur_status
        self.cur_status = new_status
        print(self)

    def __repr__(self):
        return 'Client状态： {} -> {}'.format(STATUS_REPR[self.prev_status], STATUS_REPR[self.cur_status])


class Config:
    # --------------socket相关配置-----------------
    buf_size = 1024
    host = '127.0.0.1'
    port = 50005
    # --------------client相关配置-----------------
    nick_name = '小明'
    game_id = 1
