# coding:utf-8
"""
模拟客户端操作
"""
import random
import atexit

from agent import ClientAgent
from config import *


def main():
    """客户端主程序，调用各种logic，修改当前的状态"""
    agent = ClientAgent()
    atexit.register(agent.close_socket)
    agent.ask_for_game()
    agent.receive()
    agent.analyse()  # 这里确保能够进入游戏
    while True:
        print('{}第{}步{}'.format('-' * 40, agent.cur_step + 1, '-' * 40))
        if agent.status.cur_status == WAIT_FOR_ACTION:
            agent.receive()
            agent.analyse()
        elif agent.status.cur_status == MY_TURN:
            if agent.cur_step > 10 and random.randint(0, 100) > 30:  # 一定步数后请求退出游戏
                agent.quit_game()
            else:
                agent.do_and_send_action()
        elif agent.status.cur_status == GAME_END:
            print('游戏结束')
            break
    agent.close_socket()


if __name__ == '__main__':
    main()
