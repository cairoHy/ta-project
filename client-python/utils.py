# coding:utf-8
import random


def gen_action_msg(game_id, side):
    """这里产生当前回合的动作(随机产生的)，实际客户端需要根据点击事件确定，记得做坐标转换"""

    game_id_node = {'game_id': game_id, 'side': side}

    def gen_internal1():
        return {
            'x': random.randint(0, 20),
            'y': random.randint(0, 20)
        }

    def gen_internal2():
        return {
            'src': {
                'x': random.randint(0, 20),
                'y': random.randint(0, 20)
            },
            'dest': {
                'x': random.randint(0, 20),
                'y': random.randint(0, 20)
            }
        }

    drop = {
        'type': 1,
        'msg': dict(gen_internal1(), **game_id_node)
    }
    # 模拟落1-7子
    drop_multi = {
        'type': 1,
        'msg': dict({i: gen_internal1() for i in range(random.randint(1, 7))}, **game_id_node)
    }
    move = {
        'type': 2,
        'msg': dict(gen_internal2(), **game_id_node)
    }
    # 模拟移动1-7子
    move_multi = {
        'type': 2,
        'msg': dict({i: gen_internal2() for i in range(random.randint(1, 7))}, **game_id_node)
    }

    # 自定义动作要在msg节点下保证game_id节点存储着game_id信息
    user_defined = {
        'msg': {
            'game_id': game_id,
            'side': side,
            '中文标签': 'hello,action',
            'some key': '其他value'
        }
    }
    res = [move, move_multi, drop, drop_multi, user_defined]
    return res[random.randint(0, len(res) - 1)]


def enc(msg, encoding='utf-8'):
    """编码为utf-8格式的byte类型"""
    return msg.encode(encoding)


def dec(byte_msg, encoding='utf-8'):
    """从byte类型解码为string"""
    return byte_msg.decode(encoding)


class GameInfo:
    """游戏局信息存储类"""

    def __init__(self, name=None, g_id=None, side=None):
        self.counterpart_name = name
        self.game_id = g_id
        self.side = side

    def __repr__(self):
        return '当前游戏局信息：\n\t游戏局ID：{}\n\t对手昵称：{}\n\t是否先手（红方黑方）：{}'.format(
            self.game_id, self.counterpart_name, bool(self.side)
        ) if self.game_id is not None else '当前游戏局尚未开始'
