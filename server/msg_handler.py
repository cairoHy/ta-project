# coding:utf-8
from Client import Client
import json 
import uuid 
import time 
from threading import Thread, Lock
import Queue as Queue

#游戏名称	中国象棋	围棋	五子棋	国际象棋	黑白棋	军棋	跳棋	斗兽棋	翻翻棋
#ID	1	2	3	4	5	6	7	8	9

waiting_players={}
mutex=Lock()

playing_ones={}
mutex_playing=Lock()
#init queues 
for games in range(1, 10):
	waiting_players[games]=Queue.Queue()


def get_counterpart_from(waiting_ones):
	counterpart=None
	mutex.acquire()
	if not waiting_ones.empty():
		counterpart=waiting_ones.get() 
	mutex.release()
	return counterpart

def to_wait_in_queue(client, the_queue):
	mutex.acquire()
	the_queue.put(client)
	mutex.release()

def __start_match_between(client0, client1):
	match_uuid=str(uuid.uuid4())
	mutex_playing.acquire()
	playing_ones[match_uuid]=(client0, client1)
	mutex_playing.release()

	client0.side=0
	client1.side=1

	msg0={
			"status":1,
			"counterpart_name": client1.name,
			"game_id":match_uuid,
			"side":client0.side,
		}
	msg1={
			"status":1,
			"counterpart_name": client0.name,
			"game_id":match_uuid,
			"side":client1.side,
		}
	packet0=json.dumps(msg0)
	client0.conn.send(packet0)
	packet1=json.dumps(msg1)
	client1.conn.send(packet1)



def join_game_handler(msg, addr, conn):
	new_client=Client(conn, addr, msg['name'], -1)
	game_type=msg["id"]	
	the_queue=waiting_players[game_type]
	counterpart=get_counterpart_from(the_queue)
	if not counterpart: #wait 
		to_wait_in_queue(new_client ,the_queue)
		return 
	else:
		#counterpart=get_counterpart_from(waiting_players[game_type])
		__start_match_between(new_client, counterpart)

def quit_game_handler(msg):
	match_uuid=msg['game_id']
	mutex_playing.acquire()
	pairs=playing_ones[match_uuid]
	del playing_ones[match_uuid]
	mutex_playing.release()

	if(pairs[0].side==msg['side']):
		to_notify=pairs[1]
	else:
		to_notify=pairs[0]
	msg={"game_status": "exit"}
	to_notify.conn.send(json.dumps(msg))	

def transfer_message(msg):
	match_uuid=msg['game_id']
	pairs=playing_ones[match_uuid]
	if(pairs[0].side==msg['side']):
		to_notify=pairs[1]
	else:
		to_notify=pairs[0]
	to_notify.conn.send(json.dumps(msg))