# coding:utf-8
import socket
import sys
from thread import *
import json
from msg_handler import join_game_handler, quit_game_handler, transfer_message


HOST = '127.0.0.1'	# Symbolic name meaning all available interfaces
PORT = 50005	# Arbitrary non-privileged port

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
print ('Socket created')

#try:
s.bind((HOST, PORT))
#except (socket.error, msg):
#	print ('Bind failed. Error Code : ' + str(msg[0]) + ' Message ' + msg[1])
#	sys.exit()
	
print ('Socket bind complete')
s.listen(10)
print ('Socket now listening')

def clientthread(conn, addr):
	while True:
		data= conn.recv(1024)
		if not data:
			break
		print data
		data=json.loads(data)

		if not 'type' in data:
			transfer_message(data['msg'])
			continue
		if data['type']==0:
			join_game_handler(data['msg'], addr, conn)
			continue	
		elif data['type']==3:
			quit_game_handler(data['msg'])
			break
		elif data['type']==4:
			break
		else:
			#delivering message between the two clients
			transfer_message(data['msg'])	
			continue	
	#came out of loop
	conn.close()

#now keep talking with the client
while 1:
    #wait to accept a connection - blocking call
	conn, addr = s.accept()
	print ('Connected with ' + addr[0] + ':' + str(addr[1]))
	
	#start new thread takes 1st argument as a function name to be run, second is the tuple of arguments to the function.
	start_new_thread(clientthread ,(conn, addr))

s.close()
